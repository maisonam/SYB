import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import 'vendor/rxjs';

Enzyme.configure({ adapter: new Adapter() });
