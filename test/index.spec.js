import React from 'react';
import { shallow } from 'enzyme';
import { App } from 'root/App';
import { App } from 'root/App';

jest.mock('redux-persist/lib/integration/react', () => ({
  PersistGate: () => (<div />),
}));

describe('App', () => {

  it('loads application when user is not logged in', () => {
    const userStatus = { isAuthenticated: false };
    const component = shallow(<App user={userStatus}/>);

    expect(component.find('.app')).toHaveLength(1);

  });

});
