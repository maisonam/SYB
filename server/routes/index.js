const routes = require('express').Router();
const axios = require('axios');


routes.get('/topten', (req, res) => {

  return axios.get(`https://api.coinmarketcap.com/v2/ticker/?convert=EUR&limit=10&sort=rank&structure=array`)
    .then(function (response) {
      // handle success
      console.log(response);
      res.status(200).json(response.data);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });

});

module.exports = routes;
