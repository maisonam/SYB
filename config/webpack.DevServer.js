const webpack = require('webpack');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const paths = require('./paths');
const webpackConfig = require('./webpack.config.js');

// the clean options to use
let cleanOptions = {
  root:     paths.app,
  verbose:  true,
  dry:      false,
  allowExternal: true
}

var config = merge(webpackConfig, {
  mode: 'development',
  entry: {
    main: [
      'babel-polyfill',
      'event-source-polyfill',
      'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000',
      paths.appIndexJs
    ]
  },
  module: {
   rules: [
     {
       test: /\.js$/,
       enforce: 'pre',
       exclude: /node_modules/,
       use: ['eslint-loader']
     },   
   ]
  },
  output: {
    path: paths.dist,
    publicPath: 'http://localhost:3001/',
    filename: '[name].[hash].bundle.js'
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new CleanWebpackPlugin(paths.dist, cleanOptions),
    new HtmlWebpackPlugin({
      inject: false,
      hash: true,
      template: 'indextemp.html',
      filename: 'index.html'
    })
  ],
})

var compiler = webpack(config);

module.exports.compiler = compiler;
module.exports.config = config;
