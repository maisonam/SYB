import { Observable } from 'rxjs/Observable';

export function ajaxPost(path, body) {
  return Observable.ajax({
    url: path,
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body
  });
}

export function ajaxPut(path, body) {
  return Observable.ajax({
    url: path,
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body
  });
}

export function ajaxDelete(path) {
  return Observable.ajax({
    url: path,
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' },
  });
}

export function ajaxGet(path) {
  return Observable.ajax.getJSON(path);
}
