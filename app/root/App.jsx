import React from 'react';
import PropTypes from 'prop-types';
import { Provider, connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import Helmet from 'react-helmet';
import cx from 'classnames';
import history from 'modules/history';
import RoutePublic from 'modules/RoutePublic';
import RoutePrivate from 'modules/RoutePrivate';

import config from 'config';

import Home from 'features/home';
import Stream from 'features/Stream';
import Login from 'features/Login';
import NotFound from 'features/NotFound';

import Header from 'core-components/Header';

export class App extends React.Component {
  static propTypes = {
    app: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
  };

  render() {
    const { user } = this.props;


    return (
      <ConnectedRouter history={history}>
        <div
          className={cx('app', {
            'app--private': user.isAuthenticated,
          })}
        >
          <Helmet
            defer={false}
            htmlAttributes={{ lang: 'en' }}
            encodeSpecialCharacters={true}
            defaultTitle={config.title}
            titleTemplate={`%s | ${config.name}`}
            titleAttributes={{ itemprop: 'name', lang: 'en' }}
          />
          <Header user={user} />
          <main className="app__main">
            <Switch>
              <Route exact path="/" component={Home} />
              <RoutePublic
                component={Login}
                isAuthenticated={user.isAuthenticated}
                path="/login"
                exact
              />
              <RoutePrivate
                component={Stream}
                isAuthenticated={user.isAuthenticated}
                path="/stream"
                exact
              />
              <Route component={NotFound} />
            </Switch>
          </main>
        </div>
      </ConnectedRouter>
    );
  }
}

/* istanbul ignore next */
export default connect(
  state => ({ app: state.app, user: state.user }),
  {}
)(App);
