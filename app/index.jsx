// Polyfills
// import 'core-js/shim';
// import 'classlist-polyfill';
// import 'vendor/polyfills';
// import 'babel-pollyfill';

// Rx
import 'vendor/rxjs';

import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import App from 'root/App';
import { AppContainer } from 'react-hot-loader';

import { store, persistor } from 'store';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import 'styles/main.scss';

/* istanbul ignore next */
// if (process.env.production) {
//   require('offline-plugin/runtime').install();
// }

export function renderApp(RootComponent) {
  const target = document.getElementById('react');
  const theme = createMuiTheme();

  /* istanbul ignore next */
  if (target) {
    ReactDOM.render(
      <AppContainer>
        <Provider store={store}>
          <MuiThemeProvider theme={theme}>
            <RootComponent />
          </MuiThemeProvider>
        </Provider>
      </AppContainer>,
      target
    );
  }
}

renderApp(App);

/* istanbul ignore next  */
if (module.hot) {
  module.hot.accept(
    'root/App',
    () => renderApp(require('root/App'))
  );
}
