import { action } from 'utils/actions';

export const ACTIONS = {
  FETCH_REQUEST: 'TOPTENCRYPTO_FETCH_REQUEST',
  FETCH_SUCCESS: 'TOPTENCRYPTO_FETCH_SUCCESS',
  FETCH_FAILURE: 'TOPTENCRYPTO_FETCH_FAILURE',
  FETCH_CANCEL: 'TOPTENCRYPTO_CANCEL',
};

export const fetchTopTenCrypto = () => action(ACTIONS.FETCH_REQUEST);
export const fetchTopTenSuccess = (response) => action(ACTIONS.FETCH_SUCCESS, response);
