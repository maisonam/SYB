import { createReducer } from 'utils/helpers';
import { ACTIONS } from '../actions';

const initialState = {
  topTenList: []
};

function updateTopTen (state, action) {
    return { ...state, topTenList: action.payload.data };
}

export default {
  currencies: createReducer(initialState, {
    [ACTIONS.FETCH_REQUEST](state) {
      return { ...state, ...initialState };
    },
    [ACTIONS.FETCH_FAILURE](state, action) {
      return { ...state, error: action.payload, ...initialState };
    },
    [ACTIONS.FETCH_SUCCESS](state, action) {
      return updateTopTen(state, action);
    },
  })
};
