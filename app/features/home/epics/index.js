import { ajaxGet } from 'utils/request';
import { errorHandler } from 'core-epics/errorHandler';
import { ACTIONS, fetchTopTenSuccess } from '../actions';

const proxyurl = 'https://cors-anywhere.herokuapp.com/';

function fetchAssessmentRequest() {
  return ajaxGet(`/api/currencies/topten`)
    .map(fetchTopTenSuccess)
    .catch(errorHandler(ACTIONS.FETCH_FAILURE));
}

export function fetchTopTenCrypto(action$) {
  return action$.ofType(ACTIONS.FETCH_REQUEST)
    .switchMap(fetchAssessmentRequest);
}
