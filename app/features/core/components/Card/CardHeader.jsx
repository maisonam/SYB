import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';

import MuiCardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import red from '@material-ui/core/colors/red';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const styles = theme => ({
  avatar: {
    backgroundColor: red[500],
  },
});

const CardHeader = ({ classes, cardData: { name, symbol, rank } }) => {

  return (
      <MuiCardHeader
        avatar={
          <Avatar aria-label="Card" className={classes.avatar}>{rank}</Avatar>
        }
        action={
          <IconButton>
            <MoreVertIcon />
          </IconButton>
        }
        title={name}
        subheader={symbol}
      />
  );
};

CardHeader.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { name: 'MuiCardHeader' })(CardHeader);
