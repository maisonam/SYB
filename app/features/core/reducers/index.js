/**
 * @module Reducers/Root
 * @desc Root Reducers
 */
import currencies from 'features/home/reducers';

import app from './app';
import user from './user';


export default {
  ...app,
  ...user,
  ...currencies,
};
