/**
 * @module Epics/Root
 * @desc Root Epics
 */

import { combineEpics } from 'redux-observable';
import { fetchTopTenCrypto } from 'features/home/epics';
import { userLogin, userLogout } from './user';


export default combineEpics(
  fetchTopTenCrypto,
  userLogin,
  userLogout
);
