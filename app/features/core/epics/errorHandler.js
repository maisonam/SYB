

export const getErrorMessage = (error) => {
  let message;

  if (error.response && error.response.message) {
    ({ message } = error.response);
  } else if (error.xhr && error.xhr.response && error.xhr.response.message) {
    ({ message } = error.xhr.response);
  } else {
    ({ message } = error);
  }
  return message;
};

export const errorHandler = (failureAction, additionalActions) => error => {
  console.log('epics > errorHandler() triggered with failureAction:', failureAction,
    'and error:', error);

  let actionsToTrigger = additionalActions || [];
  if (!Array.isArray(actionsToTrigger)) {
    actionsToTrigger = [additionalActions];
  }
  return [
    {
      type: failureAction,
      payload: { message: getErrorMessage(error), status: error.status },
      error: true,
    },
    ...actionsToTrigger
  ];
};
