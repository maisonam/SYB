import React from 'react';
import PropTypes from 'prop-types';

const Stream = ({ location: { state } }) => (
  <div key="stream" className="app__stream app__route">
    <div className="app__container">
      <h1>{`You must login to view ${state ? `${state.from}` : 'this page'}`}</h1>
    </div>
  </div>
);

Stream.propTypes = {
  location: PropTypes.object.isRequired,
};

export default Stream;
