import React from 'react';
import { connect } from 'react-redux';

import config from 'config';

import { fetchTopTenCrypto } from './actions'

// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";

// @material-ui/icons
import ContentCopy from "@material-ui/icons/ContentCopy";
import Store from "@material-ui/icons/Store";
import InfoOutline from "@material-ui/icons/InfoOutline";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
import Typography from '@material-ui/core/Typography';

//Core components
import GridItem from "features/core/components/GridItem.jsx";
import Card from "features/core/components/Card/Card.jsx";
import CardHeader from "features/core/components/Card/CardHeader.jsx";
import CardIcon from "features/core/components/Card/CardIcon.jsx";
import CardContent from "features/core/components/Card/CardContent.jsx";
import CardFooter from "features/core/components/Card/CardFooter.jsx";

export class Home extends React.Component {

  componentDidMount() {
    const { OnfetchTopTenCryptoCurrencies } = this.props;
    OnfetchTopTenCryptoCurrencies();
  }

  render() {
    const { topTenList } = this.props;
    return (
      <div key="Home" className="app__home app__route">
        <h2>Top 10 Cryptocurrencies By Market Capitalization</h2>
        <Grid container spacing={16}>
          {
            topTenList.map((card, key) => {
              return  (
                <GridItem xs={1} sm={1} md={6} key={key}>
                  <Card>
                    <CardHeader cardData={card} />
                    {this.buildQuoteContent(card)}
                  </Card>
                </GridItem>
              );
            })
          }
        </Grid>
      </div>
    );
  }

  buildQuoteContent(card) {
    const { market_cap, price, volume_24h, percent_change_24h } = card.quotes.USD;
    const content = (
      <div>
        <Typography color="textSecondary">
          Market Cap: ${market_cap.toLocaleString()}
        </Typography>
        <Typography variant="headline" component="h2">
          Price: ${price.toLocaleString()}
        </Typography>
        <Typography color="textSecondary">
          Volume: ${volume_24h.toLocaleString()}
        </Typography>
        <Typography component="p">
          Percent change 24h: {percent_change_24h.toLocaleString()}%
        </Typography>
      </div>
    );
    const quote = ({...card, content});
    console.log(card, quote);

    return <CardContent cardData={quote} />;

  }
}

export default connect(
  state => ({ ...state.currencies }),
  {
    OnfetchTopTenCryptoCurrencies: fetchTopTenCrypto,
  }
)(Home);
