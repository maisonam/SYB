import React from 'react';
import PropTypes from 'prop-types';

const NotFound = ({ location: { state } }) => (
  <div key="notfound" className="app__notfound app__route">
    <div className="app__container">
      <h1>{`You must login to view ${state ? `${state.from}` : 'this page'}`}</h1>
    </div>
  </div>
);

NotFound.propTypes = {
  location: PropTypes.object.isRequired,
};

export default NotFound;
