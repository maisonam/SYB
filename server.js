const express = require('express');
const path = require('path');
// const morgan = require('morgan');
// const logger = require('./server/logger');
var openBrowser = require('react-dev-utils/openBrowser');
const routes = require('./server/routes');

const app = express();
/** Get port from environment and store in Express. */
const port = process.env.PORT || '3001';

if (process.env.NODE_ENV !== 'production') {
  const webpack = require('./config/webpack.DevServer');
  app.use(require('webpack-dev-middleware')(webpack.compiler, {
    hot: true, noInfo: true, publicPath: webpack.config.output.publicPath
  }));
  app.use(require('webpack-hot-middleware')(webpack.compiler, {
    log: console.log, path: '/__webpack_hmr', heartbeat: 10 * 1000
  }));
}

app.set('view engine', 'ejs');

app.use(express.static('dist'));
app.use('/api/currencies', routes);

app.get('/', (req, res) => {
    res.status(200).render(`${__dirname}/dist/index.html`);
});

// app.use(morgan('combined', { stream: logger.stream }));

app.listen(port, () => {
  if (openBrowser('http://localhost:3001')) {
    console.log('The browser tab has been opened!');
  }
});
